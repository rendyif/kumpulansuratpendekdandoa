package com.example.primula.kumpulansuratpendekdandoa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Surat(View view) {
        Intent intent = new Intent(MainActivity.this, SuratActivity.class);
        startActivity(intent);
    }
}
